import * as React from 'react';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import './RegistrationVerificationPage.css';

const RegistrationVerificationPage = () => {
	return (
		<div className='verification__wrapper'>
			<h3 className='verification__title'>Registration</h3>
			<CheckCircleIcon className='verification__icon' sx={{ fontSize: 60 }} />
			<div className='verification__message'>
				<p>
					Congratulations, you have successfully registered with Learning
					Platform! Here are your credentials that you can change in your
					account page.
				</p>
			</div>
			<p className='verification__subtitle'>User name</p>
			<p className='verification__data'>Marta_st</p>
			<p className='verification__subtitle'>Password</p>
			<p className='verification__data'>qwerty12345</p>
		</div>
	);
};

export default RegistrationVerificationPage;
