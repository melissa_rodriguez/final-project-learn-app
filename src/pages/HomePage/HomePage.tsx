import React, { FC } from 'react';
import './HomePage.css';
import Page from 'src/components/Page/Page';
import Button from 'src/components/Button/Button';
import { NavigateFunction, useNavigate } from 'react-router-dom';

const HomePage: FC = () => {
	const navigate: NavigateFunction = useNavigate();

	const onClickJoinUs = (): void => {
		navigate('join-us');
	};

	return (
		<Page
			title="Let's Start Learning"
			subtitle='Welcome to Learn Platform - where every day is a day to learn. Dive into the vast ocean of knowledge and power and empower yourself with the tools for a successful tomorrow. Happy learning!'
		>
			<video width='957' height='540' className='home__video' controls>
				<source src='../assets/sampleVideo.mp4' type='video/mp4' />
			</video>
			<div className='home__join-us-container'>
				<p className='home__container-title'>Join us</p>
				<p className='home__container-subtitle'>
					Qui ut exercitation officia proident enim non tempor tempor ipsum ex
					nulla ea adipisicing sit consequat enim elit cupidatat o
				</p>
				<Button
					className='home-container__button'
					buttonText='Join us'
					buttonAction={onClickJoinUs}
				/>
			</div>
		</Page>
	);
};

export default HomePage;
