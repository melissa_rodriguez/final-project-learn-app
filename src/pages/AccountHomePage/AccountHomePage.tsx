import React from 'react';
import './AccountHomePage.css';
import Page from 'src/components/Page/Page';
import Button from 'src/components/Button/Button';
import Box from 'src/components/Box/Box';

interface ArticleBox {
	imageSrc: string;
	tag: string;
	title: string;
	date: string;
	duration: string;
}

const AccountHomePage = () => {
	const accountHomeArticles: ArticleBox[] = [
		{
			imageSrc: '../assets/articleImg1.png',
			tag: 'Do consectetur',
			title: 'Aliqua Irure Tempor Lorem Occaecat Volup',
			date: 'Dec 24, 2022',
			duration: '5',
		},
		{
			imageSrc: '../assets/articleImg2.png',
			tag: 'Consequat labore',
			title: 'Commodo Deserunt Ipsum Occaecat Qui',
			date: 'Dec 12, 2022',
			duration: '10',
		},
		{
			imageSrc: '../assets/articleImg3.png',
			tag: 'Laboris nulla',
			title: 'Deserunt Cccaecat Qui Amet Tempor Dolore',
			date: 'Nov 20, 2022',
			duration: '3',
		},
	];

	return (
		<Page
			title='Hi, user!'
			subtitle='Welcome to Learn Platform - where every day is a day to learn. Dive into the vast ocean of knowledge and power and empower yourself with the tools for a successful tomorrow. Happy learning!'
		>
			<h2 className='account-home__title'>What's new?</h2>
			<p className='account-home__subtitle'>
				Do consectetur proident proident id eiusmod deserunt consequat pariatur
				ad ex velit do Lorem reprehenderit.
			</p>
			<div className='account-home__box-list'>
				{accountHomeArticles.map((box) => (
					<Box
						imageSrc={box.imageSrc}
						tag={box.tag}
						title={box.title}
						date={box.date}
						duration={box.duration}
					/>
				))}
			</div>
			<Button
				className='account-home__articles-button'
				buttonText='Read more articles'
			/>
		</Page>
	);
};

export default AccountHomePage;
