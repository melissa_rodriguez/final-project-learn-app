import React from 'react';
import JoinUsBox from 'src/components/JoinUsBox/JoinUsBox';
import Page from 'src/components/Page/Page';

const JoinUsPage = () => {
	return (
		<Page title='Join Us' subtitle=''>
			<JoinUsBox role='Trainer' imageSrc='../assets/joinUsTrainer.png' />
			<JoinUsBox role='Student' imageSrc='../assets/joinUsStudent.png' />
		</Page>
	);
};

export default JoinUsPage;
