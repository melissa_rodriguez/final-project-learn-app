import React, { useEffect } from 'react';
import './MyAccountPage.css';
import Button from 'src/components/Button/Button';
import MyAccountList from 'src/components/MyAccountList/MyAccountList';
import Page from 'src/components/Page/Page';
import CustomTable from 'src/components/Table/CustomTable';

const MyAccountPage = () => {
	useEffect(() => {
		const loadData = async () => {
			const response = await fetch(
				'https://eu7n7f9rd3.execute-api.us-east-2.amazonaws.com/dev/users/me',
				{
					method: 'GET',
					headers: {
						'Content-Type': 'application/json',
					},
				}
			);
			const userData = await response.json();
			console.log(userData);
		};
		loadData();
	}, []);

	return (
		<Page title='My Account' subtitle=''>
			<div className='my-account__profile-table'>
				<MyAccountList imageSrc='../assets/profilePicture1.png' />
				<div className='my-account__table-section'>
					<div className='my-account__table-header'>
						<p className='my-account__header-title'>My Trainers</p>
						<Button
							className='my-account__add-trainer-button'
							buttonText='Add trainer'
						/>
					</div>
					<CustomTable />
				</div>
			</div>
			<div className='my-account__button-group'>
				<Button
					className='my-account__edit-profile-button'
					buttonText='Edit profile'
				/>
				<Button
					className='my-account__change-password-button'
					buttonText='Change Password'
				/>
				<Button
					className='my-account__delete-profile-button'
					buttonText='Delete profile'
				/>
			</div>
			<p className='my-account__trainings-title'>My Trainings</p>
			<p className='my-account__trainings-subtitle'>
				The Training Section is interactive, allowing you to engage with
				trainers and fellow learners, participate in quizzes, and track your
				progress. All our courses are flexible and adaptable to your schedule
				and learning speed.
			</p>
			<Button
				className='my-account__view-training-button'
				buttonText='View trainings'
			/>
		</Page>
	);
};

export default MyAccountPage;
