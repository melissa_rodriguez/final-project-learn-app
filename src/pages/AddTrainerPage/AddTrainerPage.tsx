import React, { FC } from 'react';
import './AddTrainerPage.css';
import Page from 'src/components/Page/Page';
import Button from 'src/components/Button/Button';
import CustomTable from 'src/components/Table/CustomTable';

const AddTrainerPage: FC = () => {
	return (
		<Page title='Add trainer' showBreadcrumb>
			<div className='add-trainer__small-print'>
				<p className='add-trainer__small-print-text'>
					Please select trainers for adding them into your trainers list
				</p>
				<p className='home__small-print-text'>
					* - There is no possibility to remove the trainer.
				</p>
			</div>
			<div className='add-trainer__tables'>
				<div className='add-trainer__all-trainers-table-section'>
					<div className='add-trainers__table-header'>
						<p className='add-trainer__header-title'>All Trainers</p>
					</div>
					<CustomTable />
				</div>
				<div className='add-trainer__my-trainers-table-section'>
					<div className='add-trainers__table-header'>
						<p className='add-trainer__header-title'>My Trainers</p>
					</div>
					<CustomTable />
				</div>
			</div>
			<Button className='add-trainer__button' buttonText='Add' />
		</Page>
	);
};

export default AddTrainerPage;
