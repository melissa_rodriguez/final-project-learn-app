import React from 'react';
import './App.css';
import './index.css';
import Login from './components/Login/Login';
import Registration from './components/Registration/Registration';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import JoinUsPage from './pages/JoinUsPage/JoinUsPage';
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import HomePage from './pages/HomePage/HomePage';
import AccountHomePage from './pages/AccountHomePage/AccountHomePage';
import MyAccountPage from './pages/MyAccountPage/MyAccountPage';
import AddTrainerPage from './pages/AddTrainerPage/AddTrainerPage';
import RegistrationVerificationPage from './pages/RegistrationVerificationPage/RegistrationVerificationPage';

function App() {
	return (
		<div className='App'>
			<BrowserRouter>
				<Header />
				<Routes>
					<Route path='/' element={<HomePage />} />
					<Route path='home' element={<AccountHomePage />} />
					<Route path='login' element={<Login />} />
					<Route path='my-account' element={<MyAccountPage />} />
					<Route path='my-account/add-trainer' element={<AddTrainerPage />} />
					<Route path='training' element={<Login />} />
					<Route path='join-us' element={<JoinUsPage />} />
					<Route path='change-password' element={<Login />} />
					<Route
						path='registration/trainer'
						element={
							<Registration
								role='Trainer'
								imageSrc='../assets/registrationTrainer.png'
							/>
						}
					/>
					<Route
						path='registration/student'
						element={
							<Registration
								role='Student'
								imageSrc='../assets/registrationStudent.png'
							/>
						}
					/>
					<Route
						path='registration-verification'
						element={<RegistrationVerificationPage />}
					/>
				</Routes>
				<Footer />
			</BrowserRouter>
		</div>
	);
}

export default App;
