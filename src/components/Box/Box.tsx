import React from 'react';
import './Box.css';

const Box = (props) => {
	return (
		<div className='box__root'>
			<img
				src={props.imageSrc}
				alt='group of 5 people'
				className='box__image'
			/>
			<div className='box__text'>
				<p className='box__tag'>{props.tag}</p>
				<p className='box__title'>{props.title}</p>
			</div>
			<div className='box__additional-info'>
				<p className='box__date'>{props.date}</p>
				<p className='box__duration'>{props.duration} mins read</p>
			</div>
		</div>
	);
};

export default Box;
