import React from 'react';
import './Footer.css';
import Navigation from '../Navigation/Navigation';
import Input from '@mui/material/Input';
import InputAdornment from '@mui/material/InputAdornment';
import Divider from '@mui/material/Divider';
import Button from '../Button/Button';
import MailOutlineIcon from '@mui/icons-material/MailOutline';
import TwitterIcon from '@mui/icons-material/Twitter';
import FacebookRoundedIcon from '@mui/icons-material/FacebookRounded';
import YouTubeIcon from '@mui/icons-material/YouTube';

const productNavLinks: string[] = ['Features', 'Pricing'];
const resourcesNavLinks: string[] = ['Blog', 'Webinars'];
const companyNavLinks: string[] = ['About us', 'Contact us'];

const Footer = () => {
	const onClickSubscription = (): void => {
		alert(
			'WIP: You would get an e-mail confirming the newsletter subscription'
		);
	};

	return (
		<div className='footer__root'>
			<div className='footer__nav'>
				<img
					src='../assets/learnLogo.png'
					alt='learn logo'
					className='footer__logo'
				/>
				<div className='footer__nav-list'>
					<p className='nav-list__title'>Products</p>
					{productNavLinks.map((link) => (
						<Navigation linkTitle={link} className='nav-list__link' />
					))}
				</div>
				<div className='footer__nav-list'>
					<p className='nav-list__title'>Resources</p>
					{resourcesNavLinks.map((link) => (
						<Navigation linkTitle={link} className='nav-list__link' />
					))}
				</div>
				<div className='footer__nav-list'>
					<p className='nav-list__title'>Company</p>
					{companyNavLinks.map((link) => (
						<Navigation linkTitle={link} className='nav-list__link' />
					))}
				</div>
				<div className='subscription__form'>
					<p className='subscription__title'>Subscribe to our newsletter</p>
					<p className='subscription__subtitle'>
						For product announcements and exclusive insights
					</p>
					<Input
						className='subscribe__input'
						placeholder='Input your email'
						startAdornment={
							<InputAdornment position='start'>
								<MailOutlineIcon className='subscribe-icon' />
							</InputAdornment>
						}
					/>
					<Button
						className='subscribe-button'
						buttonText='Subscribe'
						buttonAction={onClickSubscription}
					/>
				</div>
			</div>
			<Divider />
			<div className='footer__copyright'>
				<p className='footer__privacy'>
					&#169; 2023 Learn, Inc. &#8226; Privacy &#8226; Terms
				</p>
				<div className='footer__social-media'>
					<a href='https://twitter.com/'>
						<TwitterIcon className='social-media__icon' />
					</a>
					<a href='https://www.facebook.com/'>
						<FacebookRoundedIcon className='social-media__icon' />
					</a>
					<a href='https://www.youtube.com/'>
						<YouTubeIcon className='social-media__icon' />
					</a>
				</div>
			</div>
		</div>
	);
};

export default Footer;
