import React from 'react';
import './Page.css';
import Breadcrumbs from '../Breadcrumbs/Breadcrumbs';

const Page = ({ children, title, subtitle = '', showBreadcrumb = false }) => {
	return (
		<div>
			{showBreadcrumb && <Breadcrumbs urlPathName={window.location.pathname} />}
			<h2 className='page__title'>{title}</h2>
			<p className='page__subtitle'>{subtitle}</p>
			{children}
		</div>
	);
};

export default Page;
