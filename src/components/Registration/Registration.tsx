import React, { useState, useEffect, FC } from 'react';
import Button from '../Button/Button';
import './Registration.css';
import { NavigateFunction, useNavigate } from 'react-router-dom';

interface RegistrationProps {
	role: string;
	imageSrc: string;
}

interface RegistrationInputs {
	firstName?: string;
	lastName?: string;
	email?: string;
	specialization?: string;
	dateOfBirth?: string;
	address?: string;
}

const StudentRegistrationInitialVals = {
	firstName: '',
	lastName: '',
	email: '',
};

const TrainerRegistrationInitialVals = {
	firstName: '',
	lastName: '',
	email: '',
	specialization: '',
};

const Registration: FC<RegistrationProps> = ({ role, imageSrc }) => {
	const [inputVals, setInputVals] = useState<RegistrationInputs>({});
	const [inputErrors, setInputErrors] = useState<RegistrationInputs>({});
	const [showErrors, setShowErrors] = useState<boolean>(false);
	const navigate: NavigateFunction = useNavigate();

	useEffect(() => {
		if (role === 'Student') {
			setInputVals(StudentRegistrationInitialVals);
		} else if (role === 'Trainer') {
			setInputVals(TrainerRegistrationInitialVals);
		}
	}, []);

	const onChange = (e) => {
		const { name, value } = e.target;
		setInputVals((prevVals) => ({ ...prevVals, [name]: value }));
	};

	const onBlur = () => {
		if (showErrors) validateInputs();
	};

	const validateInputs = () => {
		let isValid = true;
		const newErrors = {};
		console.log(inputVals);
		Object.keys(inputVals).forEach(function (key) {
			if (inputVals[key].length > 0) {
				newErrors[key] = false;
			} else {
				isValid = false;
				newErrors[key] = true;
			}
		});
		setInputErrors(newErrors);
		return isValid;
	};

	const onSubmit = async (e) => {
		e.preventDefault();
		if (!validateInputs()) {
			setShowErrors(true);
		} else {
			const response = await fetch(
				'https://eu7n7f9rd3.execute-api.us-east-2.amazonaws.com/dev/auth/register',
				{
					method: 'POST',
					body: JSON.stringify({ ...inputVals, role }),
					headers: {
						'Content-Type': 'application/json',
					},
				}
			);
			const data = await response.json();
			//const user = JSON.parse(data.input);
			//console.log(data.input);
			navigate('/registration-verification');
		}
	};

	return (
		<div className='registration__root'>
			<div>
				<h2 className='registration__title'>Registration</h2>
				<h3 className='registration__role'>{role}</h3>
			</div>
			<div className='registration__content'>
				<img
					alt={`${role} user`}
					src={imageSrc}
					className='registration__image'
				/>
				<form className='registration__form' onSubmit={onSubmit}>
					<label className='registration__label'>First name</label>
					<input
						className={`registration__input${
							inputErrors.firstName ? '--error' : ''
						}`}
						onChange={onChange}
						onBlur={onBlur}
						name='firstName'
						value={inputVals.firstName}
					/>
					<label className='registration__label'>Last name</label>
					<input
						className={`registration__input${
							inputErrors.lastName ? '--error' : ''
						}`}
						onChange={onChange}
						onBlur={onBlur}
						name='lastName'
						value={inputVals.lastName}
					/>
					<label className='registration__label'>Email</label>
					<input
						className={`registration__input${
							inputErrors.email ? '--error' : ''
						}`}
						onChange={onChange}
						onBlur={onBlur}
						name='email'
						value={inputVals.email}
					/>
					{role === 'Trainer' && (
						<div>
							<label className='registration__label'>Specialization</label>
							<select
								className={`registration__input${
									inputErrors.specialization ? '--error' : ''
								}`}
								onChange={onChange}
								onBlur={onBlur}
								name='specialization'
								value={inputVals.specialization}
							>
								<option value=''></option>
								<option value='php'>PHP</option>
								<option value='javascript'>Javascript</option>
								<option value='python'>Python</option>
								<option value='java'>Java</option>
							</select>{' '}
						</div>
					)}
					{role === 'Student' && (
						<div>
							<label className='registration__label'>
								Date of Birth (optional)
							</label>
							<input
								className={`registration__input${
									inputErrors.dateOfBirth ? '--error' : ''
								}`}
								onChange={onChange}
								onBlur={onBlur}
								name='dateOfBirth'
								value={inputVals.dateOfBirth}
							/>
						</div>
					)}
					{role === 'Student' && (
						<div>
							<label className='registration__label'>Address (optional)</label>
							<input
								className={`registration__input${
									inputErrors.address ? '--error' : ''
								}`}
								onChange={onChange}
								onBlur={onBlur}
								name='address'
								value={inputVals.address}
							/>
						</div>
					)}
					<Button className='registration__button' buttonText='Submit' />
				</form>
			</div>
		</div>
	);
};

export default Registration;
