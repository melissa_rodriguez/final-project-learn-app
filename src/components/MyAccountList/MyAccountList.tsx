import React from 'react';
import './MyAccountList.css';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';

const MyAccountList = (props) => {
	return (
		<div className='my-profile__root'>
			<p className='my-profile__title'>My profile</p>
			<div className='my-profile__img-status'>
				<img
					src={props.imageSrc}
					alt='profile picture'
					className='my-profile__image'
				/>
				<div className='my-profile__status'>
					<b>Status</b>
					<p className='my-profile__status--check'>
						<CheckCircleOutlineIcon /> Active
					</p>
				</div>
			</div>
			<div className='my-profile__user-info'>
				<b className='my-profile__info-label'>First Name</b>
				<p className='my-profile__info-text'>Marta</p>
				<b className='my-profile__info-label'>Last Name</b>
				<p className='my-profile__info-text'>Black</p>
				<b className='my-profile__info-label'>User Name</b>
				<p className='my-profile__info-text'>Marta_st</p>
				<b className='my-profile__info-label'>Date of birth</b>
				<p className='my-profile__info-text'>01.01.2001</p>
				<b className='my-profile__info-label'>Address</b>
				<p className='my-profile__info-text'>
					123 Main Street, Boston, MA, 02108, United States
				</p>
				<b className='my-profile__info-label'>Email</b>
				<p className='my-profile__info-text'>marta_12334@gmail.com</p>
			</div>
		</div>
	);
};

export default MyAccountList;
