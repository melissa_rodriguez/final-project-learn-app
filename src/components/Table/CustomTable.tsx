import * as React from 'react';
import Table, { tableClasses } from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import './CustomTable.css';
import { styled } from '@mui/material/styles';

function createData(name: string, status: string) {
	return { name, status };
}

const StyledTableCell = styled(TableCell)(({ theme }) => ({
	[`&.${tableCellClasses.head}`]: {
		backgroundColor: theme.palette.common.black,
		color: theme.palette.common.white,
	},
	[`&.${tableCellClasses.body}`]: {
		fontSize: 14,
	},
}));

const StyledTable = styled(Table)(() => ({
	[`&.${tableClasses.root}`]: {
		borderColor: 'red',
		borderSpacing: '5px',
	},
}));

const rows = [
	createData('Elizabeth Lopez', 'Active'),
	createData('Mathew Martinez', 'Active'),
	createData('Elizabeth Hall', 'Not Active'),
	createData('Maria White', 'Not Active'),
	createData('Elizabeth Watson', 'Not Active'),
	createData('Elizabeth Allen', 'Not Active'),
	createData('Caleb Jones', 'Not Active'),
	createData('Elizabeth Lopez', 'Active'),
	createData('Mathew Martinez', 'Active'),
	createData('Elizabeth Hall', 'Not Active'),
	createData('Maria White', 'Not Active'),
	createData('Elizabeth Watson', 'Not Active'),
	createData('Elizabeth Allen', 'Not Active'),
	createData('Caleb Jones', 'Not Active'),
];

const CustomTable = () => {
	return (
		<TableContainer component={Paper}>
			<StyledTable aria-label='simple table'>
				<TableHead>
					<TableRow>
						<StyledTableCell>Name</StyledTableCell>
						<StyledTableCell>Status</StyledTableCell>
					</TableRow>
				</TableHead>
				<TableBody>
					{rows.map((row) => (
						<TableRow
							key={row.name}
							// sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
						>
							<StyledTableCell component='th' scope='row'>
								{row.name}
							</StyledTableCell>
							<StyledTableCell>{row.status}</StyledTableCell>
						</TableRow>
					))}
				</TableBody>
			</StyledTable>
		</TableContainer>
	);
};

export default CustomTable;
