import React, { FC, useEffect, useState } from 'react';
import './Breadcrumbs.css';
import { Link } from 'react-router-dom';

interface BreadcrumbsProps {
	urlPathName: string;
}

const Breadcrumbs: FC<BreadcrumbsProps> = ({ urlPathName }) => {
	const [crumbLinks, setCrumbLinks] = useState<string[]>([]);
	const [currentCrumb, setCurrentCrumb] = useState<string>();

	useEffect(() => {
		const urlArrays = urlPathName.split('/');
		setCurrentCrumb(urlArrays.pop());
		setCrumbLinks(urlArrays.slice(1));
	}, [urlPathName]);

	const formatBreadcrumb = (crumb) => {
		if (crumb)
			return crumb
				.split('-')
				.map((str) => str.charAt(0).toUpperCase() + str.slice(1))
				.join(' ');
	};

	const formatLink = (linkIndex) => {
		let finalLink = '';
		for (let i = 0; i <= linkIndex; i++) {
			finalLink += `/${crumbLinks[i]}`;
		}
		return finalLink;
	};

	return (
		<div className='breadcrumb__root'>
			{crumbLinks.map((link, i) => (
				<div>
					<Link className='crumbLink' to={formatLink(i)}>
						{formatBreadcrumb(link)}
					</Link>
					<b>{' > '}</b>
				</div>
			))}
			<span className='current-crumb'>{formatBreadcrumb(currentCrumb)}</span>
		</div>
	);
};

export default Breadcrumbs;
