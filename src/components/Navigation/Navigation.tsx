import React from 'react';
import './Navigation.css';

const Navigation = (props) => {
	return (
		<p className={props.className} onClick={props.buttonAction}>
			{props.linkTitle}
		</p>
	);
};

export default Navigation;
