import React, { FC, useState } from 'react';
import './Login.css';
import Button from '../Button/Button';
import PersonOutlineIcon from '@mui/icons-material/PersonOutline';
import Input from '@mui/material/Input';
import InputAdornment from '@mui/material/InputAdornment';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import { Link } from 'react-router-dom';
import Reaptcha from 'reaptcha';

interface LoginInputs {
	username?: string;
	password: string;
}

const loginInitialVals = {
	username: '',
	password: '',
};

const Login: FC = () => {
	const [inputVals, setInputVals] = useState<LoginInputs>(loginInitialVals);

	const onChange = (e) => {
		const { name, value } = e.target;
		console.log({ e, name, value });
		setInputVals((prevVals) => ({ ...prevVals, [name]: value }));
	};

	const onSubmit = async (e) => {
		e.preventDefault();

		const response = await fetch(
			'https://eu7n7f9rd3.execute-api.us-east-2.amazonaws.com/dev/auth/login',
			{
				method: 'POST',
				body: JSON.stringify(inputVals),
				headers: {
					'Content-Type': 'application/json',
				},
			}
		);
		//console.log(response);
		const data = await response.json();
		console.log(data);
	};
	return (
		<div className='login-root'>
			<h2 className='signin__text'>Sign In</h2>
			<p className='signin__subtext'>Welcome back</p>
			<div className='login__form'>
				<form className='input__items' onSubmit={onSubmit}>
					<label>User name</label>
					<Input
						className='login-input'
						placeholder='Enter user'
						onChange={onChange}
						name='username'
						startAdornment={
							<InputAdornment position='start'>
								<PersonOutlineIcon className='login-icon' />
							</InputAdornment>
						}
					/>
					<label>Password</label>
					<Input
						className='login-input'
						type='password'
						placeholder='Enter password'
						onChange={onChange}
						name='password'
						startAdornment={
							<InputAdornment position='start'>
								<LockOutlinedIcon className='login-icon' />
							</InputAdornment>
						}
					/>
					<Button className='signin-button' buttonText='Sign In' />
				</form>
			</div>
			<p className='or-text'>OR</p>
			<p className='alternate-question-text'>
				Don't have an account?{' '}
				<Link className={'signup-link'} to='/join-us'>
					Sign up
				</Link>
			</p>
			<Reaptcha
				sitekey='6LcsMkUpAAAAAN6M__PXZPagh_aNzs9PKbsfE9Mg'
				className='recaptcha'
			/>
		</div>
	);
};

export default Login;
