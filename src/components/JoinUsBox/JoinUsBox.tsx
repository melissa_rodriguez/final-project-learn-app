import React, { FC } from 'react';
import './JoinUsBox.css';
import Button from '../Button/Button';
import { NavigateFunction, useNavigate } from 'react-router-dom';

interface JoinUsBoxProps {
	role: string;
	imageSrc: string;
}

const JoinUsBox: FC<JoinUsBoxProps> = (props) => {
	const navigate: NavigateFunction = useNavigate();

	const onClickJoinUs = (): void => {
		navigate(`/registration/${props.role.toLowerCase()}`);
	};

	return (
		<div className='join-us-box__root'>
			<div className='join-us__info'>
				<p className='join-us__title'>Register as {props.role}</p>
				<p className='join-us__subtitle'>
					Do consectetur proident proident id eiusmod deserunt consequat
					pariatur ad ex velit do Lorem reprehenderit.
				</p>
				<Button
					className='join-us__button'
					buttonText='Join us'
					buttonAction={onClickJoinUs}
				/>
			</div>
			<img
				src={props.imageSrc}
				alt='group of 5 people'
				className='join-us__image'
			/>
		</div>
	);
};

export default JoinUsBox;
