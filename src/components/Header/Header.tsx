import React, { FC } from 'react';
import './Header.css';
import Navigation from '../Navigation/Navigation';
import Button from '../Button/Button';
import { Link, NavigateFunction, useNavigate } from 'react-router-dom';

const headerNavLinks: string[] = ['Blog', 'Pricing', 'About Us'];

const Header: FC = () => {
	const navigate: NavigateFunction = useNavigate();

	const onClickJoinUs = (): void => {
		navigate('join-us');
	};

	return (
		<div className='header-root'>
			<div className='logo-navbar'>
				<img
					src='../assets/learnLogo.png'
					alt='learn logo'
					className='header-logo'
				/>
				{window.location.pathname !== '/login' && (
					<div className='header__nav-list'>
						{headerNavLinks.map((link) => (
							<Navigation linkTitle={link} className='header-nav-list__link' />
						))}
					</div>
				)}
			</div>
			{window.location.pathname === '/' && (
				<div className='header__signin-joinus'>
					<Link className={'header__sign-in-text'} to='/login'>
						Sign in
					</Link>
					<Button
						className='header__join-us-button'
						buttonText='Join us'
						buttonAction={onClickJoinUs}
					/>
				</div>
			)}
		</div>
	);
};

export default Header;
